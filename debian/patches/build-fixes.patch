commit 67fc01b0f5591e67498799c6eb953fe4fe20e9b7
Author: Stephen Kitt <steve@sk2.org>
Date:   Fri Jan 8 14:19:45 2021 +0100

    Build system improvements (#20)
    
    * Clean up whitespace in Makefile
    
    Signed-off-by: Stephen Kitt <steve@sk2.org>
    
    * Use $(easyloggingdir) as the include directory
    
    Signed-off-by: Stephen Kitt <steve@sk2.org>
    
    * Install to $(DESTDIR)
    
    Common practice for installation targets is to install to $(DESTDIR),
    if it's defined. This is used by most packaging systems to install for
    packaging purposes (instead of the final destination directories).
    
    Signed-off-by: Stephen Kitt <steve@sk2.org>
    
    * Use CXX instead of CC
    
    Since the build uses C++, use $(CXX) and $(CXXFLAGS) instead of $(CC)
    and $(CFLAGS); the latter are used for the C compiler.
    
    Signed-off-by: Stephen Kitt <steve@sk2.org>
    
    * Allow the build flags to be overridden
    
    Packaging systems use CXX, CXXFLAGS, LDFLAGS etc. to define standard
    flags used for package builds. This patch changes the build to
    preserve the values of those externally-defined flags, augmenting them
    if necessary.
    
    Signed-off-by: Stephen Kitt <steve@sk2.org>

diff --git a/Makefile b/Makefile
index 23d91f0..a89d044 100644
--- a/Makefile
+++ b/Makefile
@@ -1,6 +1,6 @@
-CC=g++
-CFLAGS=-Wall -ansi -D_FILE_OFFSET_BITS=64 -DFUSE_USE_VERSION=26 -DELPP_SYSLOG -DELPP_NO_DEFAULT_LOG_FILE -DELPP_THREAD_SAFE -std=c++11 `xml2-config --cflags` 
-LDFLAGS=-Wall -ansi -lpcre -lfuse `xml2-config --libs` -lpthread
+CXX?=g++
+CXXFLAGS+=-Wall -ansi -D_FILE_OFFSET_BITS=64 -DFUSE_USE_VERSION=26 -DELPP_SYSLOG -DELPP_NO_DEFAULT_LOG_FILE -DELPP_THREAD_SAFE -std=c++11 `xml2-config --cflags`
+LDFLAGS+=-Wall -ansi -lpcre -lfuse `xml2-config --libs` -lpthread
 srcdir=src
 easyloggingdir=vendor/github.com/muflihun/easyloggingpp/src
 builddir=build
@@ -11,33 +11,32 @@ $(builddir):
 	mkdir $(builddir)
 
 loggedfs: $(builddir)/loggedfs.o $(builddir)/Config.o $(builddir)/Filter.o $(builddir)/easylogging.o
-	$(CC) -o loggedfs $(builddir)/loggedfs.o $(builddir)/Config.o $(builddir)/Filter.o $(builddir)/easylogging.o $(LDFLAGS)
+	$(CXX) $(CPPFLAGS) -o loggedfs $(builddir)/loggedfs.o $(builddir)/Config.o $(builddir)/Filter.o $(builddir)/easylogging.o $(LDFLAGS)
 
 $(builddir)/loggedfs.o: $(builddir)/Config.o $(builddir)/Filter.o $(srcdir)/loggedfs.cpp
-	$(CC) -o $(builddir)/loggedfs.o -c $(srcdir)/loggedfs.cpp $(CFLAGS) -Ivendor/github.com/muflihun/easyloggingpp/src
+	$(CXX) $(CPPFLAGS) -o $(builddir)/loggedfs.o -c $(srcdir)/loggedfs.cpp $(CXXFLAGS) -I$(easyloggingdir)
 
 $(builddir)/Config.o: $(builddir)/Filter.o $(srcdir)/Config.cpp $(srcdir)/Config.h
-	$(CC) -o $(builddir)/Config.o -c $(srcdir)/Config.cpp $(CFLAGS)
+	$(CXX) $(CPPFLAGS) -o $(builddir)/Config.o -c $(srcdir)/Config.cpp $(CXXFLAGS)
 
 $(builddir)/Filter.o: $(srcdir)/Filter.cpp $(srcdir)/Filter.h
-	$(CC) -o $(builddir)/Filter.o -c $(srcdir)/Filter.cpp $(CFLAGS)
+	$(CXX) $(CPPFLAGS) -o $(builddir)/Filter.o -c $(srcdir)/Filter.cpp $(CXXFLAGS)
 
 $(builddir)/easylogging.o: $(easyloggingdir)/easylogging++.cc $(easyloggingdir)/easylogging++.h
-	$(CC) -o $(builddir)/easylogging.o -c $(easyloggingdir)/easylogging++.cc $(CFLAGS)	
+	$(CXX) $(CPPFLAGS) -o $(builddir)/easylogging.o -c $(easyloggingdir)/easylogging++.cc $(CXXFLAGS)
 
 clean:
 	rm -rf $(builddir)/
-	
+
 install:
-	gzip --keep loggedfs.1
-	cp loggedfs.1.gz /usr/share/man/man1/
-	cp loggedfs /usr/bin/
-	cp loggedfs.xml /etc/
+	mkdir -p $(DESTDIR)/usr/share/man/man1 $(DESTDIR)/usr/bin $(DESTDIR)/etc
+	gzip < loggedfs.1 > $(DESTDIR)/usr/share/man/man1/loggedfs.1.gz
+	cp loggedfs $(DESTDIR)/usr/bin/
+	cp loggedfs.xml $(DESTDIR)/etc/
 
 
 mrproper: clean
 	rm -rf loggedfs
-			
+
 release:
 	tar -c --exclude="CVS" $(srcdir)/ loggedfs.xml LICENSE loggedfs.1.gz Makefile | bzip2 - > loggedfs.tar.bz2
-
